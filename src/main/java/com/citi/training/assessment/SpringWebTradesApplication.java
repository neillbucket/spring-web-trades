package com.citi.training.assessment;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringWebTradesApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringWebTradesApplication.class, args);
	}

}
