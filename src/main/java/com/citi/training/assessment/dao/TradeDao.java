/**
 * 
 */
package com.citi.training.assessment.dao;

import java.util.List;

import com.citi.training.assessment.model.Trade;

/**
 * @author Administrator
 *
 */
public interface TradeDao {
	
    List<Trade> findAll();

    Trade findById(int id);

    Trade create(Trade trade);

    void deleteById(int id);
}
