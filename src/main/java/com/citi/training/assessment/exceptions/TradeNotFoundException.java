/**
 * 
 */
package com.citi.training.assessment.exceptions;

/**
 * Exception thrown when no trade matching id is found
 * @author Administrator
 *
 */
@SuppressWarnings("serial")
public class TradeNotFoundException extends RuntimeException{
	public TradeNotFoundException(String message) {
		super(message);
	}
}
