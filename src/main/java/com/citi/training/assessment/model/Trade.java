/**
 * 
 */
package com.citi.training.assessment.model;

/**
 * @author Administrator
 *
 */
public class Trade {
	
	private int id;
	private String stock;
	private double price;
	private int volume;
	
	/**
	 * Default constructor
	 */
	public Trade() {
		
	}
	
	/**
	 * Constructor with args but no id
	 * @param stock
	 * @param price
	 * @param volume
	 */
	public Trade(String stock, double price, int volume) {
		this(-1, stock, price, volume);
	}
	
	/**
	 * Constructor with args including id
	 * @param id
	 * @param stock
	 * @param price
	 * @param volume
	 */
	public Trade(int id, String stock, double price, int volume) {
		this.id = id;
		this.stock = stock;
		this.price = price;
		this.volume = volume;
	}
	
	/**
	 * @return the id
	 */
	public int getId() {
		return id;
	}
	/**
	 * @param id the id to set
	 */
	public void setId(int id) {
		this.id = id;
	}
	/**
	 * @return the stock
	 */
	public String getStock() {
		return stock;
	}
	/**
	 * @param stock the stock to set
	 */
	public void setStock(String stock) {
		this.stock = stock;
	}
	/**
	 * @return the price
	 */
	public double getPrice() {
		return price;
	}
	/**
	 * @param price the price to set
	 */
	public void setPrice(double price) {
		this.price = price;
	}
	/**
	 * @return the volume
	 */
	public int getVolume() {
		return volume;
	}
	/**
	 * @param volume the volume to set
	 */
	public void setVolume(int volume) {
		this.volume = volume;
	}
	
	/**
	 * To string
	 */
	@Override
	public String toString() {
		return "Trade [id=" + id + ", stock=" + stock + ", price=" + price + ", volume=" + volume + "]";
	}
	
	
	
	
}
