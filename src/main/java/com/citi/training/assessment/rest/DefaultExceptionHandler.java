/**
 * 
 */
package com.citi.training.assessment.rest;

import javax.annotation.Priority;
import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import com.citi.training.assessment.exceptions.TradeNotFoundException;

/**
 * Default exception handler with logging
 * @author Administrator
 *
 */
@ControllerAdvice
@Priority(1)
public class DefaultExceptionHandler {
	 
	private static final Logger logger = LoggerFactory.getLogger(DefaultExceptionHandler.class);
	    
	    @ExceptionHandler(value = {TradeNotFoundException.class})
	    public ResponseEntity<Object> tradeNotFoundExceptionHandler(
	            HttpServletRequest request, TradeNotFoundException ex) {
	        logger.warn(ex.toString());
	        return new ResponseEntity<>("{\"message\": \"" + ex.getMessage() + "\"}",
	                                    HttpStatus.NOT_FOUND);
	    }
}
