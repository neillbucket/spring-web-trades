/**
 * 
 */
package com.citi.training.assessment.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.citi.training.assessment.dao.TradeDao;
import com.citi.training.assessment.model.Trade;


/**
 * @author Administrator
 *
 */
@Component
public class TradeService {
	@Autowired
	private TradeDao tradeDao;
	
	public List<Trade> findAll() {
		return tradeDao.findAll();

	}
	
	public Trade findById(int id) {
		return tradeDao.findById(id);
	}
	

	public Trade create(Trade trade) {
		if (trade.getStock().length() > 0) {
			return tradeDao.create(trade);
		} else {
			throw new RuntimeException("Invalid Parameter: stock trade: " + trade.getStock());
		}
	}

	// This on is void as it deletes and does not return anything
	public void deleteById(int id) {
		tradeDao.deleteById(id);
	}

}
