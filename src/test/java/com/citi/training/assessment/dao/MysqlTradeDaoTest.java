/**
 * 
 */
package com.citi.training.assessment.dao;

import static org.junit.Assert.assertEquals;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import com.citi.training.assessment.model.Trade;


/**
 * test class for MysqlTradeDao run on h2 database
 * @author Administrator
 *
 */
@RunWith(SpringRunner.class)
@SpringBootTest
@ActiveProfiles("h2")
public class MysqlTradeDaoTest {
	 @Autowired
	    MysqlTradeDao mysqlTradeDao;

	    @Test
	    @Transactional
	    public void test_createAndFindAll() {
	        mysqlTradeDao.create(new Trade(-1, "AAPL", 10.0, 100));

	        assertEquals(1, mysqlTradeDao.findAll().size());
	    }

	}

