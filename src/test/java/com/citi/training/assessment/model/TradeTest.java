package com.citi.training.assessment.model;

import static org.junit.Assert.*;

import org.junit.Test;


public class TradeTest {
	
	//Test data
	private int testId = 53;
    private String testStock = "AAPL";
    private double testPrice = 25000.00;
    private int testVolume = 100;
    
    @Test
    public void test_Trade_constructor() {
        Trade testTrade = new Trade(testId, testStock, testPrice, testVolume);

        assertEquals(testId, testTrade.getId());
        assertEquals(testStock, testTrade.getStock());
        assertEquals(testPrice, testTrade.getPrice(), 0.0001);
    }

    @Test
    public void test_Trade_toString() {
        String testString =  new Trade(testId, testStock, testPrice, testVolume).toString();

        assertTrue(testString.contains((new Integer(testId)).toString()));
        assertTrue(testString.contains(testStock));
        assertTrue(testString.contains(String.valueOf(testPrice)));
        assertTrue(testString.contains(String.valueOf(testVolume)));
    }
}


